# Шаблонные алгоритмы

---
# Шаблоны

```c++
void F() {
    std::vector<int> v1;
    std::vector<std::string> v2;
    std::sort(v1.begin(), v1.end());
    std::sort(v2.begin(), v2.end());
}
```

---
# Шаблонные функции

```c++
int max(int a, int b) {
    return (a < b) ? b : a;
}

void f() {
    int x = 10, y = 4;
    std::cout << max(x, y) << std::endl;
}
```

---
# Шаблонные функции

```c++
int MyMax(int a, int b) {
    return (a < b) ? b : a;
}

void F() {
    int x = 10, y = 4;
    std::cout << MyMax(x, y) << std::endl;
}

void G() {
    double x = 10.1, y = 3.9;
    std::cout << MyMax(x, y) << std::endl; // out: 10
}
```

---
# Шаблонные функции

```c++
int MyMax(int a, int b) {
    return (a < b) ? b : a;
}

double MyMax(double a, double b) {
    return (a < b) ? b : a;
}

void F() {
    int x = 10, y = 4;
    std::cout << MyMax(x, y) << std::endl;
}

void G() {
    double x = 10.1, y = 3.9;
    std::cout << MyMax(x, y) << std::endl; // out: 10.1
}
```

---
# Шаблонные функции

```c++
template<class T>
T MyMax(T a, T b) {
    return (a < b) ? b : a;
}

void F() {
    int x = 10, y = 4;
    std::cout << MyMax(x, y) << std::endl;
}

void G() {
    double x = 10.1, y = 3.9;
    std::cout << MyMax(x, y) << std::endl; // out: 10.1
}
```

---
# Шаблонные функции

```c++
template<class T>
T MyMax(T a, T b) {
    return (a < b) ? b : a;
}

void F() {
    int x = 10, y = 4;
    std::cout << MyMax(x, y) << std::endl;
}

void G() {
    double x = 10.1, y = 3.9;
    std::cout << MyMax(x, y) << std::endl; // out: 10.1
}

void H() {
    double x = 10.1;
    iny y = 4;
    std::cout << MyMax(x, y) << std::endl; // error:
}
```


---
# Шаблонные функции

```c++
template<class T>
T MyMax(T a, T b) {
    return (a < b) ? b : a;
}

void F() {
    int x = 10, y = 4;
    std::cout << MyMax(x, y) << std::endl;
}

void G() {
    double x = 10.1, y = 3.9;
    std::cout << MyMax(x, y) << std::endl; // out: 10.1
}

void H() {
    double x = 10.1;
    iny y = 4;
    std::cout << MyMax<double>(x, y) << std::endl;
}
```

---
# Шаблонные функции stl

 * `std::min`, `std::max`, `std::swap`
 * `std::sort`, `std::copy`, `std::lower_bound`

---
# Шаблонные функции: `std::min`

```c++
template<class T>
T min(T a, T b) {
    if (b < a) {
        return b;
    }
    return a;
}
```

  * Чем эта реализация отличается от `stl`?

---
# Шаблонные функции: `std::min`

```bash
$ cd /usr/include/c++/7
$ ag 'min\('
$ less bits/stl_algobase.h
```

```c++
  template<typename _Tp>
    _GLIBCXX14_CONSTEXPR
    inline const _Tp&
    min(const _Tp& __a, const _Tp& __b)
    {
      if (__b < __a)
        return __b;
      return __a;
    }
```

---
# Шаблонные функции: `std::swap`

---
# Шаблонные функции: `std::swap`

```c++
template<class T>
void swap(T a, T b) {
    T tmp;
    tmp = a;
    a = b;
    b = tmp;
}
```

---
# Шаблонные функции: `std::swap`

```c++
template<class T>
void swap(T& a, T& b) {
    T tmp;
    tmp = a;
    a = b;
    b = tmp;
}
```

---
# Шаблонные функции: `std::swap`

```bash
$ cd /usr/include/c++/7
$ less bits/move.h
```

```c++
  template<typename _Tp>
    inline
    typename enable_if<__and_<__not_<__is_tuple_like<_Tp>>,
                              is_move_constructible<_Tp>,
                              is_move_assignable<_Tp>>::value>::type
    swap(_Tp& __a, _Tp& __b)
    noexcept(__and_<is_nothrow_move_constructible<_Tp>,
                    is_nothrow_move_assignable<_Tp>>::value)
    {
      _Tp __tmp = _GLIBCXX_MOVE(__a);
      __a = _GLIBCXX_MOVE(__b);
      __b = _GLIBCXX_MOVE(__tmp);
    }
```

---
# Шаблонные функции: `std::swap`

![](03-stl-and-lambdas/chan2.jpg)

---
# Шаблонные функции: `std::swap`

```c++
template<class T> // <- typename вместо class
void swap(T& a, T& b) {
    T tmp; // <- У типа должен быть default конструктор
    tmp = a; // <- Лишние копирования
    a = b;
    b = tmp;
}
```

---
# Шаблонные функции: `std::swap`

```c++
template<class T>
void swap(T& a, T& b) {
    T tmp = std::move(a);
    a = std::move(b);
    b = std::move(tmp);
}
```

---
# Шаблонные функции: `std::sort`

```c++
template<???>
void sort(???);
```

---
# Шаблонные функции: `std::sort`

```c++
template<class Iterator>
void sort(???);
```

---
# Шаблонные функции: `std::sort`

```c++
template<class Iterator>
void sort(Iterator begin, Iterator end);
```

---
# Шаблонные функции: `std::sort`

```c++
template<class Iterator>
void sort(Iterator begin, Iterator end);

template<class Iterator, class Compare>
void sort(Iterator begin, Iterator end, Compare cmp);
```

---
# Шаблонные функции: `std::sort`

```c++
template<class Iterator>
void sort(Iterator begin, Iterator end);

template<class Iterator, class Compare>
void sort(Iterator begin, Iterator end,
          const Compare& cmp); // ???
```

---
# Шаблонные функции: `std::sort`

```c++
  template<typename _RandomAccessIterator,
           typename _Compare>
    inline void
    sort(_RandomAccessIterator __first,
         _RandomAccessIterator __last,
         _Compare __comp)
    {
      std::__sort(__first, __last,
        __gnu_cxx::__ops::__iter_comp_iter(__comp));
    }
```

---
# Итераторы

```

  +---+---+---+---+---+
  | 0 | 1 | 2 | 3 | 4 | *
  +---+---+---+---+---+ |
    |   |               |
    +---+---------------+--------  begin
        |               |
        +---------------+--------  begin + 1
                        |
                        +--------  end
                        
```

---
# Reverse

 *  Попробуем написать функцию Reverse

```c++
template<class Iterator>
void Reverse(Iterator begin, Iterator end) {
    size_t distance = end - begin;
    for (size_t i = 0; i < distance / 2; i++) {
        swap(*(begin + i), *(end - i - 1));
    }
}
```

---
# Reverse

```
user: Функция Reverse не работает!
you: Не может такого быть!
you: Мы её протестировали вдоль и поперёк.
user: In file included from /usr/local/include/c++/4.9.4/bits/stl_algobase.h:67:0,
                 from /usr/local/include/c++/4.9.4/list:60,
                 from list.cpp:1:
/usr/local/include/c++/4.9.4/bits/stl_iterator.h:1128:5: note: template<class _Iterator> decltype ((__x.base() - __y.base())) std::operator-(const std::move_iterator<_Iterator>&, const 
     operator-(const move_iterator<_Iterator>& __x,
     ^
/usr/local/include/c++/4.9.4/bits/stl_iterator.h:1128:5: note:   template argument deduction/substitution failed:
list.cpp:7:27: note:   ‘std::_List_iterator<int>’ is not derived from ‘const std::move_iterator<_Iterator>’                           ^
```

---
# Reverse

```c++
std::list<int> l = {1, 2, 3};
Reverse(l.begin(), l.end());
```

---
# Unique

```c++
template<class Iterator, class OutputIterator>
void Unique(Iterator begin, Iterator end,
            OutputIterator out) {
    if (begin != end) {
        *out = *begin;
        begin++;
    }
    
    while (begin != end) {
        if (*begin != *out) {
            out++;
        }
        
        *out = *begin;
        begin++;
    }
}
```

---
# Unique

```
user: Функция Unique не работает!
you: ...
you: но...
you: я же...
```

---
# Unique

```c++
std::vector<int> big = ReadInput();
std::vector<int> output;

Unique(big.begin(), big.end(),
    std::back_inserter(output));
```

---
# back_inserter

```c++
class back_inserter {
private:
    std::vector<int>& container;

public:
    back_inserter& operator* () {
        return *this;
    }

    back_inserter& operator = (int value) {
        container.push_back(value);
        return *this;
    }
};
```

---
# Итераторы

 * Есть несколько разных сущностей.
 * Все они прикидываются итераторами.

## Категории итераторов

 * RandomAccess
 * Bidirectional
 * Input
 * Output

---
# Итераторы

## RandomAccess

 * Работает как итератор вектора

## Bidirectional
 * Нельзя делать `begin - end`
 * Нельзя делать `begin + 4`

## Input
 * Можно делать `auto value = *it`

## Output
 * Можно делать `*it = value`

---
# Лямбда функции

```c++
std::vector<int> x;
std::vector<int> y;

void IsEven(int x) {
    return x % 2 == 0;
}

std::copy_if(x.begin(), x.end(), back_inserter(y),
    IsEven);
```

---
# Лямбда функции

```c++
std::copy_if(x.begin(), x.end(), back_inserter(y),
    IsSasdfaewqweRdsfasdf);
```

---
# Лямбда функции

```c++
std::copy_if(x.begin(), x.end(), back_inserter(y),
    [] (int value) {
        return value == 42
            || (value % 3 == 0
                && value > HorillaBananaConsumption);
    });
```

---
# Синтаксис лямбда функций

```c++
auto predicate =
   [/* capture */]
   (/* arguments */)
   mutable /* optional mutable */
   -> void /* optional return value */
   { }; /* body */

[] () {} (); /* valid c++ statement */
```

---
# Синтаксис лямбда функций

```c++
[] (int x) { return x % 2 == 0; };
[] { return 42; }

std::vector<int> v;
[&] (int x) {
    return std::lower_bound(v.begin(), v.end(), x)
        != v.end();
};

[&] (int x) mutable {
    v.push_back(x);
};
```
