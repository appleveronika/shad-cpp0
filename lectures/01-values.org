* Другой взгляд

** Значения

int x = 1;
int y = 2;

** Передача параметров и локальные переменные

void f(int z) {
    int y = z + 1;
}

int x = 5;
f(x);

** Указатели

int x = 5;
int* p = nullptr;
p = &x;

x = 6
*p = 10;

** Передача параметров по указателю

void f(int* z) {
  *z += 1;
}

int x = 0;
f(&x);

** Use after return

int* f() {
  int x = 1;
  return &x;
}

int* p = f();
*p = 5;

** Ссылки

int x = 5;
int& y = x;
const& z = y;

y = 6;
auto p = &z;

** Как работает Вектор

struct vector {
  int* begin;
  int* end;
  int* capacity;
};

std::vector<int> x;
x.resize(5);
x[4] = 10;

** Копирование вектора

std::vector<int> x = {1, 2, 3};
std::vector<int> y = x;

** Передача вектора по значению

void Foo(std::vector<int> y) {
    y.clear();
}

std::vector<int> x = {1, 2, 3};

Foo(x);

x.size();

** Передача вектора по указателю

void Bar(std::vector<int>* y) {
    y->clear();
}

std::vector<int> x = {1, 2, 3};

Bar(x);

x.size();

** Элементы вектора и арифметика указателей

std::vector<int> x = {1, 2, 3};
int* p = x[0];
p += 1;
*p = 4;

int& v = x[0];

** RVO

std::vector<int> Generate() {
   std::vector<int> y = {1, 2, 3};
   return y;
}

std::vector<int> x = Generate();

** Move вектора

std::vector<int> x = {1, 2, 3};

std::vector<int> y = std::move(x);
