#include <catch.hpp>

#include <itertools.h>
#include <vector>
#include <list>
#include <iostream>
#include <deque>

TEST_CASE("Range 1") {
    int i = 0;
    for (auto val : Range(5)) {
        REQUIRE(val == i++);
    }
    REQUIRE(i == 5);
}

TEST_CASE("Range 2") {
    int i = 2;
    for (auto val : Range(2, 7, 2)) {
        REQUIRE(val == i);
        i += 2;
    }
    REQUIRE(i == 8);
}

TEST_CASE("Range huge") {
    int64_t i = 1;
    for (auto val : Range(1ll, 1ll << 61)) {
        REQUIRE(i == val);
        ++i;
        if (i == 100) {
            break;
        }
    }
    REQUIRE(i == 100);
}

TEST_CASE("Zip equal") {
    std::vector<int> first{1, 3, 5};
    std::list<int> second{6, 4, 2};
    auto i1 = first.begin();
    auto i2 = second.begin();
    for (const auto& val : Zip(first, second)) {
        REQUIRE(val.first == *i1);
        REQUIRE(val.second == *i2);
        ++i1;
        ++i2;
    }
    REQUIRE(i1 == first.end());
    REQUIRE(i2 == second.end());
}

TEST_CASE("Zip short") {
    std::string s("abacaba");
    auto it = s.begin();
    int64_t i = 0;

    for (const auto& val : Zip(s, Range(1ll << 62))) {
        REQUIRE(val.first == *it);
        REQUIRE(val.second == i++);
        ++it;
    }
    REQUIRE(it == s.end());
    REQUIRE(i == s.size());
}

struct Int {
    int x;
    Int(int val) : x(val) {
    }
    Int(const Int&) = delete;
    Int& operator=(const Int&) = delete;
    Int(Int&&) = delete;
    Int& operator=(Int&&) = delete;

    bool operator==(const Int& rhs) const {
        return x == rhs.x;
    }
};

TEST_CASE("Zip noncopy") {

    std::deque<Int> first;
    first.emplace_back(5);
    first.emplace_back(4);
    first.emplace_back(3);

    int i = 0;
    auto it = first.begin();
    for (const auto& val : Zip(Range(3), first)) {
        REQUIRE(val.first == i++);
        REQUIRE(val.second == *it);
        ++it;
    }
    REQUIRE(i == 3);
    REQUIRE(it == first.end());
}

TEST_CASE("Group small") {
    std::vector<int> data{1, 1, 2, 2, 2, 3};
    std::vector<std::vector<int>> expected{{1, 1}, {2, 2, 2}, {3}};

    size_t i = 0;

    for (const auto& val : Group(data)) {
        auto it = expected[i].begin();
        for (const auto& elem : val) {
            REQUIRE(elem == *it);
            ++it;
        }
        REQUIRE(it == expected[i].end());
        ++i;
    }
    REQUIRE(i == expected.size());
}

TEST_CASE("Group empty") {
    std::vector<int> empty;
    int i = 0;
    for (const auto& val : Group(empty)) {
        std::ignore = val;
        ++i;
    }
    REQUIRE(i == 0);
}

TEST_CASE("Group non copy") {
    std::list<Int> data;
    data.emplace_back(1);
    data.emplace_back(2);
    data.emplace_back(2);

    int total = 2;

    for (const auto& val : Group(data)) {
        --total;
        int key = (*val.begin()).x;
        int i = 0;
        for (const auto& elem : val) {
            REQUIRE(elem.x == key);
            ++i;
        }
        REQUIRE(i == key);
    }
    REQUIRE(total == 0);
}
