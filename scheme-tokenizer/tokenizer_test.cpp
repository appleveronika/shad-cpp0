#include <catch.hpp>

#include <tokenizer.h>

#include <sstream>

TEST_CASE("Tokenizer works on simple case") {
    std::stringstream ss{"4+)'."};
    Tokenizer tokenizer{&ss};

    REQUIRE(!tokenizer.IsEnd());
    // Confused by compilation error? Think harder!
    REQUIRE(tokenizer.GetToken() == Token{Constant{4}});

    tokenizer.Next();
    REQUIRE(!tokenizer.IsEnd());
    REQUIRE(tokenizer.GetToken() == Token{Symbol{"+"}});

    tokenizer.Next();
    REQUIRE(!tokenizer.IsEnd());
    REQUIRE(tokenizer.GetToken() == Token{Bracket::CLOSE});

    tokenizer.Next();
    REQUIRE(!tokenizer.IsEnd());
    REQUIRE(tokenizer.GetToken() == Token{Quote{}});

    tokenizer.Next();
    REQUIRE(!tokenizer.IsEnd());
    REQUIRE(tokenizer.GetToken() == Token{Dot{}});

    tokenizer.Next();
    REQUIRE(tokenizer.IsEnd());
}

TEST_CASE("Negative numbers") {
    std::stringstream ss{"-2 - 2"};
    Tokenizer tokenizer{&ss};


    REQUIRE(!tokenizer.IsEnd());
    REQUIRE(tokenizer.GetToken() == Token{Constant{-2}});

    tokenizer.Next();
    REQUIRE(!tokenizer.IsEnd());
    REQUIRE(tokenizer.GetToken() == Token{Symbol{"-"}});

    tokenizer.Next();
    REQUIRE(!tokenizer.IsEnd());
    REQUIRE(tokenizer.GetToken() == Token{Constant{2}});
}

TEST_CASE("GetToken is not moving") {
    std::stringstream ss{"4+4"};
    Tokenizer tokenizer{&ss};

    REQUIRE(tokenizer.GetToken() == Token{Constant{4}});
    REQUIRE(tokenizer.GetToken() == Token{Constant{4}});
}

TEST_CASE("Tokenizer is streaming") {
    std::stringstream ss;
    ss << "2 ";

    Tokenizer tokenizer{&ss};
    REQUIRE(tokenizer.GetToken() == Token{Constant{2}});

    ss << "*";
    tokenizer.Next();
    REQUIRE(tokenizer.GetToken() == Token{Symbol{"*"}});

    ss << "2";
    tokenizer.Next();
    REQUIRE(tokenizer.GetToken() == Token{Constant{2}});
}

TEST_CASE("Spaces are handled correctly") {
    SECTION("Just spaces") {
        std::stringstream ss{"      "};
        Tokenizer tokenizer{&ss};

        REQUIRE(tokenizer.IsEnd());
    }

    SECTION("Spaces between tokens") {
        std::stringstream ss{"  4 +  "};
        Tokenizer tokenizer{&ss};

        REQUIRE(!tokenizer.IsEnd());
        REQUIRE(tokenizer.GetToken() == Token{Constant{4}});

        tokenizer.Next();
        REQUIRE(!tokenizer.IsEnd());
        REQUIRE(tokenizer.GetToken() == Token{Symbol{"+"}});

        tokenizer.Next();
        REQUIRE(tokenizer.IsEnd());
    }
}

TEST_CASE("Empty string handled correctly") {
    std::stringstream ss;
    Tokenizer tokenizer{&ss};

    REQUIRE(tokenizer.IsEnd());
}